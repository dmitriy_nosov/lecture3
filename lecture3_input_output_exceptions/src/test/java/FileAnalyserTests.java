import org.junit.Assert;
import org.junit.Test;
import ru.edu.lecture3.FileAnalyser;
import ru.edu.lecture3.FileAnalyserImpl;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public class FileAnalyserTests {

    private FileAnalyser fileAnalyser = null;

    @Test
    public void fileAnalyserTest() {
    }

    /**
     * Get file name test
     *
     * @throws IOException
     */
    @Test
    public void getFileNameTest() throws IOException {
        FileWriter writer = new FileWriter("inputTest.txt");
        writer.write("Test String");
        writer.close();

        FileAnalyserImpl fileAnalyserImpl = new FileAnalyserImpl("inputTest.txt");
        String fileFromMethod = fileAnalyserImpl.getFileName();
        String fileName = "inputTest.txt";

        Assert.assertEquals(fileName, fileFromMethod);
    }

    /**
     * Test 1-row file content
     *
     * @throws IOException
     */
    @Test
    public void getRowsCountTest() throws IOException {
        FileWriter writer = new FileWriter("inputTest.txt");
        writer.write("Test String");
        writer.close();

        FileAnalyserImpl fileAnalyserImpl = new FileAnalyserImpl("inputTest.txt");

        int result = fileAnalyserImpl.getRowsCount();
        Assert.assertEquals(result, 1);
    }

    /**
     * Test amount of letters in file
     *
     * @throws IOException
     */
    @Test
    public void getLettersCountTest() throws IOException {
        FileWriter writer = new FileWriter("getLettersCountTest.txt");
        writer.write("Test String");
        writer.close();

        FileAnalyserImpl fileAnalyserImpl = new FileAnalyserImpl("getLettersCountTest.txt");

        int result = fileAnalyserImpl.getLettersCount();
        Assert.assertEquals(result, 10);
    }

    /**
     * Test Symbols Statistics
     *
     * @throws IOException
     */
    @Test
    public void getSymbolsStatisticsTest() throws IOException {
        FileWriter writer = new FileWriter("getSymbolsStatisticsTest.txt");
        writer.write("Test String");
        writer.close();

        FileAnalyserImpl fileAnalyserImpl = new FileAnalyserImpl("getSymbolsStatisticsTest.txt");

        Map<Character, Long> symbol = fileAnalyserImpl.getSymbolsStatistics();
        String result = symbol.toString();

        Assert.assertTrue(result.contains("t=2"));
    }

    @Test
    public void getTopNPopularSymbolsTest() throws IOException {

        FileWriter writer = new FileWriter("getTopNPopularSymbolsTest.txt");
        writer.write("tessstingggg");
        writer.close();

        FileAnalyserImpl fileAnalyserImpl = new FileAnalyserImpl("getTopNPopularSymbolsTest.txt");
        List<Character> symbol = fileAnalyserImpl.getTopNPopularSymbols(2);

        Assert.assertEquals('g', (char) symbol.get(0));
        Assert.assertEquals('s', (char) symbol.get(1));
    }

    /**
     * Test saving info to another file
     *
     * @throws IOException
     */
    @Test
    public void saveSummaryTest() throws IOException {

        FileWriter writer = new FileWriter("input.txt");
        writer.write("Hello\nWorld");
        writer.close();

        FileAnalyserImpl fileAnalyserImpl = new FileAnalyserImpl("input.txt");
        fileAnalyserImpl.saveSummary("summary.txt");

        int rows = fileAnalyserImpl.getRowsCount();
        String fileContent = new String(Files.readAllBytes(Paths.get("summary.txt")));
        String stringToCompare = "rowsCount: " + rows;

        Assert.assertTrue(fileContent.contains(stringToCompare));
    }
}