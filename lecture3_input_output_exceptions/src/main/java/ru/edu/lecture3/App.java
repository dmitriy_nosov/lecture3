package ru.edu.lecture3;

import java.io.*;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws IOException {

        FileWriter writer = new FileWriter("input.txt");
        writer.write("Hello\nWorld");
        writer.close();

        FileAnalyserImpl fileAnalyserImpl = new FileAnalyserImpl("input.txt");
        fileAnalyserImpl.saveSummary("summary.txt");
    }
}
