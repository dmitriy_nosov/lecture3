package ru.edu.lecture3;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FileAnalyserImpl implements FileAnalyser {

    private final Map<Character, Long> map;

    int numMin = 48; // от 0 до 9 - 48-57
    int numMax = 57;
    int upperCaseLetterMin = 65; // от A до Z - 65-90
    int upperCaseLetterMax = 90;
    int lowerCaseLetterMin = 97; // от a до z - 97-122
    int lowerCaseLetterMax = 122;
    private final String filePath;

    private final int rowCount;

    public FileAnalyserImpl(String filePath) throws IOException {
        this.filePath = filePath;
        List<String> lines = Files.readAllLines(Paths.get(this.filePath));
        rowCount = lines.size();
        map = lines.stream()
                .flatMapToInt(String::chars)
                .mapToObj(c -> (char) c)
                .filter(c -> (c >= numMin && c <= numMax) || (c >= upperCaseLetterMin && c <= upperCaseLetterMax) ||
                        (c >= lowerCaseLetterMin && c <= lowerCaseLetterMax))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    @Override
    public String getFileName() {
        return filePath;
    }

    @Override
    public int getRowsCount() {
        return rowCount;
    }

    @Override
    public int getLettersCount() {
        int result = 0;
        for (Long i : map.values()) {
            result += i;
        }

        return result;
    }

    @Override
    public Map<Character, Long> getSymbolsStatistics() {
        return Collections.unmodifiableMap(map);
    }

    @Override
    public List<Character> getTopNPopularSymbols(int n) {
        return map.entrySet().stream()
                .sorted(Map.Entry.<Character, Long>comparingByValue().reversed())
                .limit(n)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    @Override
    public void saveSummary(String filePath) throws IOException {
        File file = new File("summary.txt");
        FileOutputStream fos = new FileOutputStream(file);
        PrintStream ps = new PrintStream(fos);
        ps.println("fileName: " + getFileName());
        ps.println("rowsCount: " + getRowsCount());
        ps.println("totalSymbols: " + getLettersCount());
        ps.println("symbolsStatistics: " + getSymbolsStatistics());
        ps.println("topNPopularSymbolsTest: " + getTopNPopularSymbols(3).stream().map(Object::toString).collect(Collectors.joining("', '", "{'", "'}")));
        fos.flush();
        fos.close();
    }
}