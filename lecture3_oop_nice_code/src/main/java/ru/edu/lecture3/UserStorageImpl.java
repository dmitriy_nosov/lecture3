package ru.edu.lecture3;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class UserStorageImpl implements UserStorage {

    private Map<String, User> userMap = new HashMap<>();

    @Override
    public User getUserByLogin(String login) {

        if (login == null || login.isEmpty() || login.trim().isEmpty()) throw new IllegalArgumentException();

        if (!userMap.containsKey(login)) throw new RuntimeException("Incorrect login");
        return userMap.get(login);
    }

    @Override
    public User put(User user) {

        if (!User.isValid(user)) throw new RuntimeException();
        return userMap.put(user.getLogin(), user);
    }

    @Override
    public User remove(String login) {
        if (login == null || login.isEmpty()) throw new IllegalArgumentException("Login is null");
        if (!userMap.containsKey(login)) throw new RuntimeException();
        return userMap.remove(login);
    }

    @Override
    public List<User> getAllUsers() {
        return new ArrayList(userMap.values());
    }

    @Override
    public List<User> getAllUsersByGender(Gender gender) {
        if (gender == null) throw new IllegalArgumentException();
        List<User> users = new ArrayList<>();
        for (User user: userMap.values()) {
            if (user.getGender() == gender)
                users.add(user);
        }
        return users;
    }

    @Override
    public int getUserAge(String login) {
        return getUserByLogin(login).getBirthDate().until(LocalDate.now()).getYears();
    }
}