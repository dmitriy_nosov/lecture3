package ru.edu.lecture3;

import java.time.LocalDate;

/**
 * Contains user info.
 */
public class User {

    private String login;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private Gender gender;

    public User(String login, String firstName, String lastName,
                LocalDate birthDate, Gender gender) {
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.gender = gender;
    }

    public static boolean isValid(User user) {
        return  ((user.login != null && !user.login.isEmpty())
                && (user.firstName != null && !user.firstName.isEmpty())
                && (user.lastName != null && !user.lastName.isEmpty())
                && user.birthDate != null
                && user.gender != null);
    }

    public String getLogin() {
        return login;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public Gender getGender() {
        return gender;
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                ", gender=" + gender +
                '}';
    }
}