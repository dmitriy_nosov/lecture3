package ru.edu.lecture3;

import java.time.LocalDate;

public class App {
    public static void main(String[] args) {

        User user = new User("dima89", "Dmitriy", "Nosov",
                LocalDate.of(1989,5,16), Gender.MALE);

        UserStorage storage = new UserStorageImpl();
        storage.put(user);
        System.out.println(storage.getUserAge("dima89"));
        System.out.println(user.toString());
    }
}