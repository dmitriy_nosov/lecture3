import org.junit.Assert;
import org.junit.Test;
import ru.edu.lecture3.Gender;
import ru.edu.lecture3.User;
import ru.edu.lecture3.UserStorage;
import ru.edu.lecture3.UserStorageImpl;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class UserStorageTests {

    private UserStorage userStorage = null;

    /**
     * Put new user into storage.
     */
    @Test
    public void userStorageTest() {

        UserStorage userStorage = new UserStorageImpl();
        User user = new User("dima89", "Dmitriy", "Nosov",
                LocalDate.of(1989,5,16), Gender.MALE);
        userStorage.put(user);

        Assert.assertNotNull(userStorage);
    }

    /**
     * Get user by login
     */
    @Test
    public void getUserByLoginPositiveTest() {

        UserStorage userStorage = new UserStorageImpl();
        User user = new User("dima89", "Dmitriy", "Nosov",
                LocalDate.of(1989, 5, 16), Gender.MALE);
        userStorage.put(user);

        User userFromStorage = userStorage.getUserByLogin(user.getLogin());

        Assert.assertNotNull(userFromStorage);
    }

    /**
     * Get user with incorrect login
     */
    @Test(expected = RuntimeException.class)
    public void getUserByLoginNotExistingUserTest() {
        UserStorage userStorage = new UserStorageImpl();
        userStorage.getUserByLogin("testLogin");
    }

    /**
     * Get user with null login
     */
    @Test(expected = IllegalArgumentException.class)
    public void getUserByLoginNullUserTest() {
        UserStorage userStorage = new UserStorageImpl();
        userStorage.getUserByLogin(null);
    }

    /**
     * Get user with blank login
     */
    @Test(expected = IllegalArgumentException.class)
    public void getUserByLogin_blank_user_Test() {
        UserStorage userStorage = new UserStorageImpl();
        userStorage.getUserByLogin(" ");
    }

    /**
     * Get user with empty login
     */
    @Test(expected = IllegalArgumentException.class)
    public void getUserByLoginEmptyUserTest() {
        UserStorage userStorage = new UserStorageImpl();
        userStorage.getUserByLogin("");
    }

    /**
     * Put user with null login
     */
    @Test(expected = RuntimeException.class)
    public void putUserWithNullLoginTest() {
        UserStorage userStorage = new UserStorageImpl();
        User user = new User(null, "Dmitriy", "Nosov",
                LocalDate.of(1989, 5, 16), Gender.MALE);
        userStorage.put(user);
    }

    /**
     * Put user with incorrect date of birth (month)
     */
    @Test(expected = DateTimeException.class)
    public void putUserWithIncorrectDateOfBirthTest() {
        UserStorage userStorage = new UserStorageImpl();
        User user = new User("dima89", "Dmitriy", "Nosov",
                LocalDate.of(1989, 5, 116), Gender.MALE);
        userStorage.put(user);
    }

    /**
     * Remove user from storage
     */
    @Test(expected = RuntimeException.class)
    public void removeUserPositiveTest() {
        UserStorage userStorage = new UserStorageImpl();
        User user = new User("testLogin", "Dmitriy", "Nosov",
                LocalDate.of(1989, 5, 16), Gender.MALE);
        userStorage.put(user);

        userStorage.remove("testLogin");

        Assert.assertNull(userStorage.getUserByLogin("testLogin"));
    }

    /**
     * Remove user with null login from storage
     */
    @Test(expected = RuntimeException.class)
    public void removeUserNullLoginTest() {
        UserStorage userStorage = new UserStorageImpl();
        User user = new User(null, "Dmitriy", "Nosov",
                LocalDate.of(1989, 5, 16), Gender.MALE);
        userStorage.put(user);

        userStorage.remove(user.getLogin());
    }

    /**
     * Get all users from storage
     */
    @Test
    public void getAllUsers() {
        UserStorage userStorage = new UserStorageImpl();
        User user1 = new User("user1", "Dmitriy", "Nosov",
                LocalDate.of(1989, 5, 16), Gender.MALE);
        User user2 = new User("user2", "Vadim", "Polozov",
                LocalDate.of(1991, 4, 1), Gender.MALE);
        userStorage.put(user1);
        userStorage.put(user2);

        List<User> list = new ArrayList(userStorage.getAllUsers());

        Assert.assertEquals(2, list.size());
    }

    /**
     * Get users from storage by gender
     */
    @Test
    public void getAllUsersByGender() {
        UserStorage userStorage = new UserStorageImpl();
        User user = new User("dima89", "Dmitriy", "Nosov",
                LocalDate.of(1989, 5, 16), Gender.MALE);
        User user2 = new User("user2", "Vadim", "Polozov",
                LocalDate.of(1991, 4, 1), Gender.MALE);
        User user3 = new User("user3", "Mariya", "Detkova",
                LocalDate.of(1995, 3, 21), Gender.FEMALE);
        userStorage.put(user);
        userStorage.put(user2);
        userStorage.put(user3);

        List<User> maleUsers = userStorage.getAllUsersByGender(Gender.MALE);
        List<User> femaleUsers = userStorage.getAllUsersByGender(Gender.FEMALE);

        Assert.assertNotNull(maleUsers);
        Assert.assertNotNull(femaleUsers);
        Assert.assertEquals(maleUsers.size(), 2);
        Assert.assertEquals(femaleUsers.size(), 1);
    }

    /**
     * Check user's age calculating
     */
    @Test
    public void getUserAge() {
        UserStorage userStorage = new UserStorageImpl();
        User user = new User("dima89", "Dmitriy", "Nosov",
                LocalDate.of(1989, 5, 16), Gender.MALE);
        userStorage.put(user);

        long difference = ChronoUnit.YEARS.between(user.getBirthDate(), LocalDate.now());

        Assert.assertNotNull(difference);
    }
}